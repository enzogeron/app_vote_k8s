kubectl create namespace appvotacion

kubectl apply -f . --namespace=appvotacion

Escalar deployments

kubectl scale deployment vote --replicas=5 --namespace=appvotacion
kubectl scale deployment result --replicas=5 --namespace=appvotacion

Eliminar recursos

kubectl delete all --all --namespace=appvotacion

kubectl delete namespace appvotacion

kubectl get all --namespace=appvotacion
